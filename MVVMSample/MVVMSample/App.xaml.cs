﻿using GalaSoft.MvvmLight.Ioc;
using MVVMSample.Common;
using MVVMSample.ViewModels;
using MVVMSample.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MVVMSample
{
    public partial class App : Application
    {
        private static ViewModelLocator _locator;

        NavigationService navigationService;

        public static ViewModelLocator Locator
        {
            get
            {
                return _locator ?? (_locator = new ViewModelLocator());
            }
        }

        public App()
        {
            InitializeComponent();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            try
            {
                if (navigationService == null)
                {
                    navigationService = new NavigationService();
                }

                navigationService.Configure(ViewModelLocator.Page1Key, typeof(Page1));
                navigationService.Configure(ViewModelLocator.Page2Key, typeof(Page2));
                navigationService.Configure(ViewModelLocator.Page3Key, typeof(Page3));

                if (!SimpleIoc.Default.IsRegistered<INavigationServiceExtension>())
                {
                    SimpleIoc.Default.Register<INavigationServiceExtension>(() => navigationService);
                }


                var _firstPage = new NavigationPage(new Page1());
                navigationService.Initialize(_firstPage);

                MainPage = _firstPage;
            }
            catch (Exception ex)
            {
                //ex.Log(this.GetType().FullName, throwIfDebug: false);
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
