﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Views;

namespace MVVMSample.Common
{
    public interface INavigationServiceExtension : INavigationService
    {
        void ClearBackStackAndGotoPage(string pageKey, object parameter = null);
        void SetNavStackToLoginPage();
        Task GoHome(string homeKey);
        void PushModal(string pageKey, object parameter = null);
        void NavigateTo(string pageKey, object parameter, bool checkCache);

        Task PopAllModals();
        Task<bool> LogoutAsync();

        void RemovePageCache(string pageKey);
    }
}
