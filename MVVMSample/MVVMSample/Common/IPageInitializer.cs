﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMSample.Common
{
    public interface IPageInitializer
    {
        void Initialize(object parm);
        void ReInitialize(object parm);
    }
}
