﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMSample.Common
{
    public interface IPageLifetime
    {
        void CleanupPage();
    }
}
