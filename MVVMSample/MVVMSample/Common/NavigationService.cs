﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MVVMSample.Common
{
    public class NavigationService : INavigationServiceExtension
    {
        private readonly Dictionary<string, Type> _pagesByKey = new Dictionary<string, Type>();
        private readonly Dictionary<string, Page> _navBarPages = new Dictionary<string, Page>();

        public NavigationPage _navigation;

        SemaphoreSlim _navigationSemaphore = new SemaphoreSlim(1, 1);

        public NavigationService()
        {
        }

        public void Initialize(NavigationPage navigation)
        {
            _navigation = navigation;
        }

        public void Configure(string pageKey, Type pageType)
        {
            lock (_pagesByKey)
            {
                if (_pagesByKey.ContainsKey(pageKey))
                    _pagesByKey[pageKey] = pageType;
                else
                    _pagesByKey.Add(pageKey, pageType);
            }
        }

        public string CurrentPageKey
        {
            get
            {
                lock (_pagesByKey)
                {
                    if (_navigation.CurrentPage == null)
                        return null;

                    var pageType = _navigation.CurrentPage.GetType();

                    return _pagesByKey.ContainsValue(pageType) ? _pagesByKey.First(p => p.Value == pageType).Key : null;
                }
            }
        }

        public async void GoBack()
        {
            await _navigationSemaphore.WaitAsync();

            try
            {
                if (_navigation.Navigation.NavigationStack.Count > 2)
                    await _navigation.PopAsync();
            }
            catch (Exception ex)
            {
                //ex.Log(this.GetType().FullName);
            }
            finally
            {
                _navigationSemaphore.Release();
            }
        }

        public async Task GoHome(string homeKey)
        {
            await _navigationSemaphore.WaitAsync();

            try
            {
                var currentBackStack = _navigation.Navigation.NavigationStack.ToList();

                // The first entry should be login page
                // The second entry should be either AssociateHomePage or ManagerHomePage
                // So, we can clear all the data and make sure that only two entries are available.
                if (currentBackStack.Count > 2)
                {
                    for (int index = 2; index < currentBackStack.Count - 1; index++)
                    {
                        _navigation.Navigation.RemovePage(currentBackStack[index]);
                    }

                    await _navigation.PopAsync();
                }
            }
            finally
            {
                _navigationSemaphore.Release();
            }
        }

        public async Task<bool> LogoutAsync()
        {
            await _navigationSemaphore.WaitAsync();

            try
            {
                var currentBackStack = _navigation.Navigation.NavigationStack.ToList();

                if (currentBackStack.Count > 1)
                {
                    _navBarPages.Clear();
                    await _navigation.PopToRootAsync();
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                _navigationSemaphore.Release();
            }
        }

        public void PushModal(string pageKey, object parameter = null)
        {
            lock (_pagesByKey)
            {
                if (_pagesByKey.ContainsKey(pageKey))
                {
                    var type = _pagesByKey[pageKey];

                    try
                    {
                        Page page = parameter == null ?
                             Activator.CreateInstance(type) as Page
                            : Activator.CreateInstance(type, new object[] { parameter }) as Page;

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            _navigation.Navigation.PushModalAsync(page);
                        });
                    }
                    catch (Exception ex)
                    {
                        //ex.Log(this.GetType().FullName);
                    }
                }
                else
                {
                    throw new ArgumentException(
                        string.Format(
                            "No such page:c {-}. Did you forget to call NavigationService.Configure?",
                            pageKey),
                        "pageKey");
                }
            }
        }

        public async Task PopAllModals()
        {
            while (_navigation.Navigation.ModalStack.Count > 0)
            {
                await _navigation.Navigation.PopModalAsync();
            }
        }

        public void ClearBackStackAndGotoPage(string pageKey, object parameter = null)
        {
            try
            {
                NavigateTo(pageKey, parameter, true);
                var currentBackStack = _navigation.Navigation.NavigationStack.ToList();

                // The first entry should be login page
                // The second entry should be either AssociateHomePage or ManagerHomePage
                // So, we can clear all the data and make sure that only two entries are available.

                if (currentBackStack.Count > 2)
                {
                    for (int index = 2; index < currentBackStack.Count - 1; index++)
                    {
                        _navigation.Navigation.RemovePage(currentBackStack[index]);
                    }
                }
            }
            catch (Exception ex)
            {
                //ex.Log(this.GetType().FullName);
            }
        }

        public void SetNavStackToLoginPage()
        {
            var currentBackStack = _navigation.Navigation.NavigationStack.ToList();

            // The first entry should be login page, this method will reset it so we can have a proper backstack after store selection/assignment
            if (currentBackStack.Count > 1)
            {
                for (int index = 1; index < currentBackStack.Count - 1; index++)
                {
                    _navigation.Navigation.RemovePage(currentBackStack[index]);
                }
            }
        }

        //public void NavigateToNextQuestion(string tempPageKey, object nextQuestion)
        //{
        //	NavigateTo(tempPageKey, nextQuestion);

        //	//remove previous question from the back
        //	var currentBackStack = _navigation.Navigation.NavigationStack.ToList();
        //	var pagesCount = currentBackStack.Count;
        //	var previousQuestionType = currentBackStack[pagesCount - 2].GetType().Name;

        //	if (previousQuestionType == tempPageKey)
        //	{
        //		_navigation.Navigation.RemovePage(currentBackStack[pagesCount - 2]);
        //	}
        //}

        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null, false);
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            NavigateTo(pageKey, parameter, false);
        }

        public void NavigateTo(string pageKey, object parameter, bool checkCache)
        {
            lock (_pagesByKey)
            {
                if (CurrentPageKey == pageKey && pageKey != "TemperaturePage" && pageKey != "CorrectiveActionsPage")
                    return;

                if (_pagesByKey.ContainsKey(pageKey))
                {
                    Page page;

                    var type = _pagesByKey[pageKey];

                    try
                    {
                        // we are caching the pages that are commonly accessed throught he nav bar to prevent constantly recreating these pages
                        // this pulls the cached versions from our dictionary
                        if (checkCache && _navBarPages.ContainsKey(pageKey))
                        {
                            if (_navBarPages[pageKey] is IPageInitializer iPageInitializer)
                                iPageInitializer.ReInitialize(parameter);

                            page = _navBarPages[pageKey];
                            _navigation.PushAsync(page);
                            return;
                        }

                        page = parameter == null ?
                             Activator.CreateInstance(type) as Page
                            : Activator.CreateInstance(type, new object[] { parameter }) as Page;

                        if (checkCache)
                        {
                            if (page is IPageInitializer iPageInitializer)
                                iPageInitializer.Initialize(parameter);

                            _navBarPages.Add(pageKey, page);
                        }

                        _navigation.PushAsync(page);
                    }
                    catch (Exception ex)
                    {
                        //ex.Log(this.GetType().FullName);
                    }
                }
                else
                {
                    throw new ArgumentException(
                        string.Format(
                            "No such page:c {-}. Did you forget to call NavigationService.Configure?",
                            pageKey),
                        "pageKey");
                }
            }
        }

        

        


        public void RemovePageCache(string pageKey)
        {
            lock (_pagesByKey)
            {
                if (_navBarPages.ContainsKey(pageKey))
                {
                    var page = _navBarPages[pageKey] as IPageLifetime;
                    if (page != null)
                        page.CleanupPage();

                    _navBarPages.Remove(pageKey);
                }
            }
        }

        private void RemovePage(Page page)
        {
            if (page is IPageLifetime iPageLifetime)
                iPageLifetime.CleanupPage();

            page.BindingContext = null;

            _navigation.Navigation.RemovePage(page);
        }
    }
}
