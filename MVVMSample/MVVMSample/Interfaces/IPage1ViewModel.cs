﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace MVVMSample.Interfaces
{
    public interface IPage1ViewModel
    {
        ICommand SendDataCommand { get; set; }
        string Name { get; set; }
    }
}
