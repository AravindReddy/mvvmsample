﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMSample.Interfaces
{
    public interface IViewModelBase
    {
        // On back navigation ,as new instance of the view is not created need to set this property if any initialization 
        // is to be triggered  on navigation
        //bool IsAppearingOnBackNavigation { get; set; }
        bool IsActivityInProgress { get; set; }

    }
}
