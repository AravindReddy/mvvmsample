﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMSample.Interfaces
{
    public interface IViewModelLocator : IViewModelBase
    {
        void RegisterViewModels();
    }
}
