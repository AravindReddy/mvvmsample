﻿using MVVMSample.Common;
using MVVMSample.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MVVMSample.ViewModels
{
    public class Page1ViewModel : IPage1ViewModel
    {
        public INavigationServiceExtension _navigationService = null;
        public ICommand SendDataCommand { get; set; }
        public string Name { get; set; }

        public Page1ViewModel(INavigationServiceExtension navigationService)
        {
            _navigationService = navigationService;
            SendDataCommand = new Command(NavigateToPage2);
        }

        void NavigateToPage2()
        {
            _navigationService.NavigateTo(ViewModelLocator.Page2Key, Name);
        }
    }
}
