﻿using MVVMSample.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Ioc;
using MVVMSample.Views;

namespace MVVMSample.ViewModels
{
    public class ViewModelLocator : IViewModelLocator
    {
      
        public const string Page1Key = "Page1";
        public const string Page2Key = "Page2";
        public const string Page3Key = "Page3";

        /// <summary>
        /// Registers all the used ViewModels, Services, etc.
        /// Initializes a new instance of the <see cref="T:MG365Mobile.ViewModel.ViewModelLocator"/> class.
        /// </summary>
        public ViewModelLocator()
        {
            
            SimpleIoc.Default.Register<IPage1ViewModel, Page1ViewModel>();
            SimpleIoc.Default.Register<IPage2ViewModel, Page2ViewModel>();
            SimpleIoc.Default.Register<IPage3ViewModel, Page3ViewModel>();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
                                                         "CA1822:MarkMembersAsStatic",
                                                         Justification = "This non-static member is needed for data binding purposes.")]

        public IPage1ViewModel Page1 => SimpleIoc.Default.GetInstance<IPage1ViewModel>();

        public IPage2ViewModel Page2 => SimpleIoc.Default.GetInstance<IPage2ViewModel>();

        public IPage3ViewModel Page3 => SimpleIoc.Default.GetInstance<IPage3ViewModel>();

        public bool IsActivityInProgress { get; set; }

        //Registering the ViewModels. This method will get invoked after logout.
        public void RegisterViewModels()
        {
           
        }

        //UnRegistering the ViewModels. This method will get invoked on logout. "LoginViewModel is not getting Unregistered it is an Initial ViewModel and also Timer activities we are registering here.
        public static void UnRegisterViewModels()
        {
            
        }

        public static void Cleanup()
        {
            //TODO: clear viewmodels
        }
    }
}
