﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMSample.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
        public string _name;
        public Page2 (string Name)
		{
            _name = Name;
			InitializeComponent ();
            labelField.Text = "Hello  " + _name;
		}
	}
}